class CreateAdminSessions < ActiveRecord::Migration

  def change
    create_table(:admin_sessions) { |table|
      table.integer(:admin_id)
      table.string(:token)

      table.timestamps(null: false)
    }

    add_foreign_key(:admin_sessions, :admins, {on_delete: :cascade})
    add_index(:admin_sessions, :token, {unique: true})
  end

end
