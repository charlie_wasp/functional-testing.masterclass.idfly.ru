require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Capitan

  class Application < Rails::Application
    config.action_view.logger = nil
    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru
    config.active_record.raise_in_transactional_callbacks = true

    config.generators.stylesheets = false
    config.generators.javascripts = false
    config.session_store(:disabled)

    config.autoload_paths += [
      Rails.root.join('app/services'),
    ]

    config.secret = ENV['SECRET']
  end

end
