class CreateUsers < ActiveRecord::Migration

  def self.up
    create_table(:users, {comment: 'пользователи'}) { |table|
      table.string(:name, {null: true, default: "", comment: 'Имя'})
      table.string(:phone, {null: true, default: "", comment: 'Email'})
      table.string(:email, {null: false, default: "", comment: 'Телефон'})
      table.string(:password, {null: true, default: "", comment: 'Пароль'})

      table.timestamps null: false
    }

    add_index(:users, :email, {unique: true})
  end

end