class AdminsController < ResourcesController

  def initialize(*args)
    @model = Admin
    return super(*args)
  end

  private

  def _params
    return params.permit(:name, :email, :password)
  end

end
