class App.RequestsFormCtrl extends App.ResourcesFormCtrl

  app.controller('RequestsFormCtrl', @factory())

  @_url = 'api/requests'
  @_redirect = 'requests'
  @_key = 'request'
