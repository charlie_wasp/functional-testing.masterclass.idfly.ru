app.directive 'tariffsSlider', () ->

  require: 'ngModel',

  link: (scope, element, attrs) ->
    scope.updateSlider = (() ->
      slider.slider('value', scope.usersCount)
      return null
    )

    slider = $(element).find('#tariffs-slider').slider({
      max: 200,
      range: 'min',
      slide: (() ->
        scope.usersCount = slider.slider('value')
        scope.$apply()
      )
    });
