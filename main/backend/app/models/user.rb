class User < ActiveRecord::Base

  validates(:email, {format: {with: /.*@.*\..*/}})

  has_many(:user_sessions)

  after_initialize(:set_secrets)
  before_save(:prepare_password)

  def set_secrets(*args)
    @secrets = SecretsService.new
  end

  def prepare_password
    if !self.password_changed?
      return
    end

    if password.instance_of?(::String)
      self.password = @secrets.generate(password)
    end
  end

  def as_json(options)
    super({except: [:password]}.merge(options))
  end

end
