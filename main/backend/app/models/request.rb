class Request < ActiveRecord::Base

  validates(:email, {format: {with: /.*@.*\..*/}})

end
