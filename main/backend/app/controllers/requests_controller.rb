class RequestsController < ApplicationController

  def create
    data = params.permit(:email, :is_help_required, :name, :phone)
    request = Request.create(data)
    render({
      json: {success: request.persisted?, errors: request.errors.to_h},
    })
  end

end
