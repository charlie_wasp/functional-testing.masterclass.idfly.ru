class StatusController < ApplicationController

  skip_before_action(:require_authorization)

  def initialize(*args)
    @admins = AdminsService.new
    return super(*args)
  end

  def status
    render({
      json: {
        admin: @admins.get_authorized_user(request.headers['X-Admin-Token']),
      }
    })
  end

end
