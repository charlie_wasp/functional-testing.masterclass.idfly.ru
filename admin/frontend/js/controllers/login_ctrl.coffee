class App.LoginCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
  ]

  app.controller('LoginCtrl', @factory())

  initialize: () ->
    @_scope.login = @_login

  _login: () =>
    @_http
      .post('api/session', @_scope.admin)
      .then((response) =>
        if !response.data.success
          @_scope.admin.errors = response.data.errors
          return

        localStorage.setItem('admin.token', response.data.token)
        @_http.defaults.headers.common['X-Admin-Token'] = response.data.token
        @_rootScope.admin = response.data.admin
        @_rootScope.adminToken = response.data.token
        @_location.path('')
      )
