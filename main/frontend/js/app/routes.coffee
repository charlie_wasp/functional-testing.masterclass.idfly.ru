app.config(($routeProvider) ->

  $routeProvider

    .when('/', {
      templateUrl: '/assets/views/index/index.html',
      controller: 'IndexCtrl',
    })

)