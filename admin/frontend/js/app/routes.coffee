app.config(($routeProvider) ->

  $routeProvider

    .when('/', {
      templateUrl: 'assets/views/index/index.html',
      controller: 'IndexCtrl',
    })

    .when('/login', {
      templateUrl: 'assets/views/admin/login.html',
      controller: 'LoginCtrl',
    })

    .when('/admins', {
      templateUrl: 'assets/views/admins/index.html',
      controller: 'AdminsIndexCtrl',
    })

    .when('/admins/create', {
      templateUrl: 'assets/views/admins/create.html',
      controller: 'AdminsFormCtrl',
    })

    .when('/admins/:id', {
      templateUrl: 'assets/views/admins/update.html',
      controller: 'AdminsFormCtrl',
    })

    .when('/users', {
      templateUrl: 'assets/views/users/index.html',
      controller: 'UsersIndexCtrl',
    })

    .when('/users/create', {
      templateUrl: 'assets/views/users/create.html',
      controller: 'UsersFormCtrl',
    })

    .when('/users/:id', {
      templateUrl: 'assets/views/users/update.html',
      controller: 'UsersFormCtrl',
    })

    .when('/requests', {
      templateUrl: 'assets/views/requests/index.html',
      controller: 'RequestsIndexCtrl',
    })

    .when('/requests/create', {
      templateUrl: 'assets/views/requests/create.html',
      controller: 'RequestsFormCtrl',
    })

    .when('/requests/:id', {
      templateUrl: 'assets/views/requests/update.html',
      controller: 'RequestsFormCtrl',
    })

)