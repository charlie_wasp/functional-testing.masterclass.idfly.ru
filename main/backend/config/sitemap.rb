host 'localhost'

sitemap :site do
  url root_url, last_mod: Time.now, change_freq: 'daily', priority: 1.0
  url events_url, last_mod: Time.now, change_freq: 'hourly', priority: 1.0
  url avia_url, last_mod: Time.now, change_freq: 'hourly', priority: 1.0
end

sitemap_for Event.all, name: :events do |event|
  url event_url(
      event.date.strftime('%Y-%m-%d'),
      event.url,
      event.date.strftime('%H:%M')
    ),
    priority: (event.recommended ? 1.0 : 0.7),
    last_mod: Time.now,
    change_freq: 'hourly'
end

sitemap_for Page.all, name: :pages do |page|
  url "/page/#{page.url}", proproty: 1.0, last_mod: page.updated_at,
    change_freq: 'weekly'
end