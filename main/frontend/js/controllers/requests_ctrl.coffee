class App.RequestsCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$http',
    '$location',
    'ModalService',
  ]

  app.controller('RequestsCtrl', @factory())

  initialize: () ->
    @_scope.request = localStorage.getItem('app.request')
    if @_scope.request != undefined
      @_scope.request = JSON.parse(@_scope.request)

    @_scope.request ||= {}

    @_scope.$watch('request', @_saveRequest, true)
    @_scope.submit = @_submit

  _submit: () =>
    @_http
      .post('api/requests', @_scope.request)
      .then(@_completeSubmit)

  _completeSubmit: (response) =>
    _.extend(@_scope.request, response.data)
    if !response.data.success
      return

    @_scope.request = {}
    @_ModalService.showModal(
      controller: (($scope, close) -> $scope.close = close),
      templateUrl: 'assets/views/index/requests/sent.html',
    )

  _saveRequest: () =>
    localStorage.setItem('app.request', JSON.stringify(@_scope.request))
