class ServiceError < StandardError

  attr_reader :errors

  def initialize(message, errors = nil)
    super(message)
    @errors = errors
  end

  def as_json(_ = nil)
    return {success: false, error: message, errors: @errors}
  end

end