app.directive 'navigation', 
($location, $anchorScroll, $window, $document, $timeout) ->
  templateUrl: '/assets/views/directives/navigation.html',
  link: (scope, element) ->  

    scope.scrollToAnchor = (anchor) ->
      scope.scrollTop = $('#'+anchor).offset().top
      $(document).scrollTop(scope.scrollTop-80)
      return true

    scope.menuCheck = (index, anchor) ->
      if scope.selectedItem == index || $location.hash() == anchor
        return true
    
    scope.getAnchors = ->
      scope.anchors = $document[0].anchors
      scope.documentHeight = $(document).height()

    $timeout(-> scope.getAnchors())

    angular.element($window).on 'scroll', ->
      scope.scrollPosition = $(document).scrollTop()+80

      angular.forEach(scope.anchors, (item, key)->
        if scope.anchors[key+1]
          if scope.scrollPosition >= item.offsetTop && 
          scope.scrollPosition < scope.anchors[key+1].offsetTop
            scope.selectedItem = key+1
          else if scope.scrollPosition < scope.anchors[0].offsetTop
            scope.selectedItem = 0
        else
          if scope.scrollPosition >= item.offsetTop
            scope.selectedItem = key+1
      )

      scope.$apply()