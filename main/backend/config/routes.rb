Rails.application.routes.draw do

  # ===
  # API
  # ===

  # status

  get '/api/status' => 'status#status'

  # user

  post '/api/user/login' => 'user#login'

  # requests

  post '/api/requests' => 'requests#create'

  # ======
  # PUBLIC
  # ======

  # index

  root 'application#default'

end
