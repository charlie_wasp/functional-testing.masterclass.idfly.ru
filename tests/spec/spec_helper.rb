require 'capybara/rspec'
require 'selenium-webdriver'

Encoding.default_external = 'UTF-8'

Capybara.register_driver(:remote) { |app|
  Capybara::Selenium::Driver.new(
    app,
    :browser => :remote,
    :url => 'http://selenium.capitan.local:4444/wd/hub',
    :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome,
  )
}

Capybara.configure { |config|
  config.run_server = false
  config.default_driver = :remote
  config.app_host = 'http://http.capitan.local'
  config.server_host = 'http.capitan.local'
  config.server_port = 80
}

RSpec.configure { |config|

  def authorizate_admin_user
    page.execute_script('localStorage.setItem(
      "admin.token",
      "beb41f6f-54e3-4e77-9589-76c0145d85ae/865a5cf5c451765cd3a53424cb47825d"
    )')
  end

  config.before(:each) {
    page.driver.browser.manage.window.resize_to(1280, 960)
  }

}
