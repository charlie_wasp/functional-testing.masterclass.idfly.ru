class App.LogoutCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
  ]

  app.controller('LogoutCtrl', @factory())

  initialize: () ->
    @_scope.logout = @_logout

  _logout: () =>
    @_http
      .delete('api/session')
      .then((response) =>
        if !response.data.success
          alert('ошибка выхода')
          return

        @_rootScope.admin = undefined
        @_rootScope.adminToken = undefined
        localStorage.setItem('admin.token', undefined)
        @_location.path('login')
      )
