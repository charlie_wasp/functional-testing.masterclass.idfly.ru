class ResourcesController < ApplicationController

  def list
    render({json: {
      @model.name.split('::').last.downcase.pluralize => @model.all,
    }})
  end

  def get
    render({json: @model.find(params[:id])})
  end

  def create
    user = @model.create(_params)
    render({json: {
      success: user.persisted?,
      errors: user.errors.to_hash,
      id: user.id,
    }})
  end

  def update
    user = @model.find(params[:id])
    success = user.update(_params)
    render({json: {success: success, errors: user.errors.to_hash}})
  end

  def destroy
    user = @model.find(params[:id])
    user.destroy
    render({json: {success: user.destroyed?, errors: user.errors.to_hash}})
  end

end
