app.directive 'infographicsWheel', () ->
  templateUrl: '/assets/views/directives/wheel.html',
  link: (scope, element) ->
    scope.wheel = {
      step: 0,
      stepValue: 72,
      container: $('#animated-circle'),
      element: $('.js-animated-circle-element'),
      rotation: 0,
      timeout: 0,
      timeInterval: 3000,
      direction: -1,
      topElement: undefined,
      topElementId: 0,
      selectedId: 0
    }

    scope.rotateCircle = (index) ->
      scope.wheel.timeout = 5000
      scope.currentElementId = index

      scope.wheel.rotation = scope.wheel.stepValue * scope.currentElementId

      if scope.lastElementId != scope.currentElementId  
        scope.wheel.container.css({ 
          transform: 'rotate('+(-scope.wheel.rotation)+'deg)' 
        })
        scope.wheel.element.css({ 
          transform: 'rotate('+scope.wheel.rotation+'deg)'
        })

        scope.wheel.element
          .find('.block-infographics__element__sub')
          .addClass('hidden')

        scope.wheel.selectedId = index

      scope.lastElementId = scope.currentElementId

    scope.autoRotation = () ->
      scope.rotationTimeout = setTimeout( ->
        scope.wheel.rotation = scope.wheel.stepValue * scope.wheel.step
        scope.wheel.timeout = 0

        scope.wheel.container.css({ 
          transform: 'rotate('+(-scope.wheel.rotation)+'deg)' 
        })
        scope.wheel.element.css({ 
          transform: 'rotate('+scope.wheel.rotation+'deg)'
        })
       
        if scope.wheel.rotation == -360
          scope.wheel.direction = 1
        else if scope.wheel.rotation == 0
          scope.wheel.direction = -1
        
        scope.wheel.step = scope.wheel.step + scope.wheel.direction
        scope.wheel.topElementId = scope.wheel.rotation / scope.wheel.stepValue

        if scope.wheel.topElementId == 5
          scope.wheel.topElementId = 0

        if scope.wheel.topElementId >= 0
          scope.wheel.selectedId = scope.wheel.topElementId
        else
          scope.wheel.selectedId = 5 + scope.wheel.topElementId

        scope.$apply()

      scope.wheel.timeout
      )

    scope.rotationInterval = setInterval( 
      -> scope.autoRotation(),
      scope.wheel.timeInterval
    )

    scope.removeTimer = () ->
      if $(window).width() < 650
        clearTimeout(scope.rotationTimeout)
        clearInterval(scope.rotationInterval)

    scope.removeTimer()
