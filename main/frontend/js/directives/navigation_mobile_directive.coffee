app.directive 'navigationMobile', () ->
  templateUrl: '/assets/views/directives/navigation_mobile.html',
  link: (scope, element) ->
    scope.scroll = (anchor) ->
      scope.scrollTop = $('#'+anchor).offset().top
      $(document).scrollTop(scope.scrollTop-20)

      scope.mobileMenu = !scope.mobileMenu