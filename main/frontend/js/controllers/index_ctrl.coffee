class App.IndexCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    'ModalService',
  ]

  app.controller('IndexCtrl', @factory())

  initialize: () ->
    @_scope.login = @_login
    @_scope.showModalRequest = @_showModalRequest
    @_scope.mobileMenu = @mobileMenu

  _login: () =>
    @_ModalService.showModal(
      controller: 'LoginCtrl',
      templateUrl: 'assets/views/index/login.html',
    )

  _showModalRequest: () =>
    @_ModalService.showModal(
      controller: 'RequestsPopupCtrl',
      templateUrl: 'assets/views/index/requests/popup.html',
    )

  wow = new WOW (
    {
      boxClass: 'wow',    
      animateClass: '',
      offset: 0,          
      mobile: true,       
      live: true
    }
  )
  wow.init()

  @_mobileMenu = false