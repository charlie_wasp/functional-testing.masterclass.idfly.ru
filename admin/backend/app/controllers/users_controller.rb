class UsersController < ResourcesController

  def initialize(*args)
    @model = User
    return super(*args)
  end

  private

  def _params
    return params.permit(:name, :email, :password, :phone)
  end

end
