@App = {}

@app = angular.module('app', [
  'ngRoute',
  'angularModalService',
])

app.run(($http, $rootScope) ->
  $http.defaults.headers.common['Content-Type'] = 'application/json'
  $http.defaults.headers.common['Accept'] = 'application/json'

  token = localStorage.getItem('user.token')
  $http.defaults.headers.common['X-User-Token'] = token

  $http
    .get('/api/status')
    .then((response) =>
      _.extend($rootScope, response.data)
    )
)

app.config ($locationProvider) ->
  $locationProvider.html5Mode(true)

