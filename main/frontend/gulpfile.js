process.on('SIGTERM', function() {
    process.exit();
});

var gulp = require('gulp');
var changed = require('gulp-changed');
var util = require('gulp-util');

function expandGlobs(files) {
    var result = [];

    for(var fileIndex in files) {
        var file = files[fileIndex];
        if(file.indexOf('*') === -1) {
            result.push(file);
            continue;
        }

        list = require('glob').sync(file)
        list.sort();

        for(var fileIndex in list) {
            var file = list[fileIndex];
            if(file.search(/\.\w+$/) === -1) {
                continue;
            }

            var target = file.replace(/\.\w+$/, '.js');

            if(result.indexOf(target) === -1) {
                result.push(target);
            }
        }
    }

    return result;
}

// app

gulp.task('render-index', function() {
    var slim = require('gulp-slim');
    var template = require('gulp-template');
    var contents = require('fs').readFileSync('config/assets.json', 'utf8');
    var assets = JSON.parse(contents);

    assets.css = expandGlobs(assets.css)
    assets.js = expandGlobs(assets.js)

    return gulp
        .src('views/index.slim')
        .pipe(template({assets: assets}))
        .pipe(slim({
            pretty: true,
            require: '/app/config/slim.rb',
        }))
        .on('error', function(message) {
            util.log(util.colors.red(message));
            this.emit('end');
        })
        .pipe(gulp.dest('build/views'));
});

gulp.task('render-views', function () {
    var slim = require('gulp-slim');
    task =
        gulp
        .src(['views/**/*', '!views/index.slim'])
        .pipe(changed('build/views', {extension: '.html'}))
        .pipe(slim({
            pretty: true,
            require: '/app/config/slim.rb',
        }))
        .on('error', function(message) {
            util.log(util.colors.red(message));
            this.emit('end');
        })
        .pipe(gulp.dest('build/views'));

    return task;
});

gulp.task('compile-css', function() {
    var sass = require('gulp-sass');
    task =
        gulp
        .src(['css/app.scss'])
        .pipe(sass())
        .on('error', function(message) {
            util.log(util.colors.red(message));
            this.emit('end');
        })
        .pipe(gulp.dest('build/css'));

    return task;
});

gulp.task('copy-img', function() {
    task =
        gulp
        .src([
            'vendor/html.landing.capitan.idfly.ru/frontend/img/**/*',
            'img/**/*',
        ])
        .pipe(changed('build/img'))
        .pipe(gulp.dest('build/img'));

    return task
});

gulp.task('compile-coffee', function() {
    var coffee = require('gulp-coffee');
    var sourcemaps = require('gulp-sourcemaps');

    task =
        gulp
        .src([
            'vendor/html.landing.capitan.idfly.ru/frontend/js/**/*.coffee',
            'js/**/*',
        ])
        .pipe(sourcemaps.init())
        .pipe(changed('build/js', {extension: '.js'}))
        .pipe(coffee())
        .on('error', function(message) {
            util.log(message);
            this.end();
        })
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/js'));

    return task
});

// vendor

gulp.task('copy-vendor-fonts', function() {
    task =
        gulp
        .src([
            'node_modules/{font-awesome,roboto-font}/{fonts,css}/**/*',
            'vendor/html.landing.capitan.idfly.ru/frontend/fonts/**/*',
            '!**/*.map',
            '!**/*.min.css',
        ])
        .pipe(gulp.dest('build/fonts'));

    return task
});

gulp.task('copy-vendor-css', function() {
    task =
        gulp
        .src([
            'node_modules/normalize-css/normalize.css',
            'vendor/jquery-ui/jquery-ui.css',
        ])
        .pipe(gulp.dest('build/css/vendor'));

    return task
});

gulp.task('copy-vendor-js', function() {
    task =
        gulp
        .src([
            'node_modules/angular/angular.js',
            'node_modules/angular-route/angular-route.js',
            'node_modules/angular-modal-service/dst/angular-modal-service.js',
            'node_modules/angular-class-coffee/build/angular-class.js',
            'node_modules/jquery/dist/jquery.js',
            'node_modules/underscore/underscore.js',
            'vendor/jquery-ui/jquery-ui.js',
            'vendor/html.landing.capitan.idfly.ru/frontend/js/vendor/**/*.js',
        ])
        .pipe(gulp.dest('build/js/vendor'));

    return task
});

// dev tasks

gulp.task('default', [
    'compile-coffee',
    'compile-css',

    'copy-img',

    'render-views',
    'render-index',

    'copy-vendor-css',
    'copy-vendor-js',
    'copy-vendor-fonts',
]);

gulp.task('_watch', function() {
    gulp.watch('js/**/*', ['compile-coffee', 'render-index']);
    gulp.watch('css/**/*', ['compile-css']);
    gulp.watch('fonts/**/*', ['copy-fonts']);
    gulp.watch('img/**/*', ['copy-img']);

    gulp.watch(
        ['views/**/*', '!views/index.slim'],
        ['render-views']
    );

    gulp.watch(
        ['views/index.slim', 'config/assets.json'],
        ['render-index']
    );
});

gulp.task('watch', ['default', '_watch']);
