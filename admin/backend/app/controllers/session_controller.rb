class SessionController < ApplicationController

  skip_before_action(:require_authorization)

  def initialize(*args)
    @admins = AdminsService.new
    return super(*args)
  end

  def create
    token, admin = @admins.authorizate(params[:email], params[:password])
    render({json: {success: true, admin: admin, token: token}})
  rescue UsersService::Error => error
    render({json: error.as_json})
  end

  def destroy
    session = @admins.get_authorized_session(request.headers['X-Admin-Token'])
    if !session
      render({json: {success: false, error: 'сессия не найдена'}})
      return
    end

    session.destroy
    render({json: {success: session.destroyed?, errors: session.errors.to_h}})
  end

end
