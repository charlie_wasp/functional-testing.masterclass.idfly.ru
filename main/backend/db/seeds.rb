if User.where(id: 1).length == 0
  User.create([
    {
      id: 1,
      name: 'Пользователь',
      phone: '+71001001010',
      email: 'user@example.com',
      password: 'password',
    },
  ])
end

if User.where(id: 2).length == 0
  User.create([
    {
      id: 2,
      name: 'Человек',
      phone: '+72002002020',
      email: 'man@example.com',
      password: 'password',
    },
  ])
end

if Admin.where(id: 1).length == 0
  Admin.create([
    {
      id: 1,
      name: 'Администратор',
      email: 'admin@example.com',
      password: 'password',
    },
  ])
end

if AdminSession.where(id: 1).length == 0
  AdminSession.create([
    {
      id: 1,
      admin_id: 1,
      token: 'beb41f6f-54e3-4e77-9589-76c0145d85ae',
    },
  ])
end

if Admin.where(id: 2).length == 0
  Admin.create([
    {
      id: 2,
      name: 'Человек',
      email: 'man@example.com',
      password: 'password',
    },
  ])
end

if Request.where(id: 1).length == 0
  Request.create([
    {
      id: 1,
      email: 'user@example.com',
      is_help_required: true,
      popup: true,
      name: 'Пользователь',
      phone: '+71001001010',
    },
  ])
end
