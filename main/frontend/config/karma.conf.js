module.exports = function(config) {
  config.set({

    basePath: '../',
    frameworks: ['jasmine', 'sinon'],

    files: [
      'node_modules/underscore/underscore.js',

      'node_modules/angular/angular.js',
      'node_modules/angular-route/angular-route.js',
      'node_modules/angular-modal-service/dst/angular-modal-service.js',

      'node_modules/angular-mocks/angular-mocks.js',

      'src/app.coffee',

      // 'src/lib/util/**/*.coffee',
      // 'src/lib/services/**/*.coffee',
      // 'src/lib/controllers/**/*.coffee',

      // 'tests/**/*.coffee',
    ],

    exclude: [],

    preprocessors: {
      '**/*.coffee': ['coffee']
    },

    coffeePreprocessor: {
      options: {
        bare: true,
        sourceMap: true,
      },

      transformPath: function(path) {
        return path.replace(/\.coffee$/, '.js')
      }
    },

    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false
  });
};
