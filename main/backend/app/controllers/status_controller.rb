class StatusController < ApplicationController

  def initialize(*args)
    @users = UsersService.new
    return super(*args)
  end

  def status
    render({
      json: {
        user: @users.get_authorized_user(request.headers['X-User-Token'])
      }
    })
  end

end
