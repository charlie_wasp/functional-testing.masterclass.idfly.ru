class CreateRequests < ActiveRecord::Migration
  def change
    create_table(:requests, {comment: 'запросы на подключение'}) { |table|
      table.string(:email, null: false, comment: 'email')

      table.boolean(:is_help_required, {
        null: false,
        default: false,
        comment: 'требуется помощь при подключении'
      })

      table.boolean(:popup, {
        null: false,
        default: false,
        comment: 'заявка оставлена через попап'
      })

      table.string(:name, null: true, comment: 'name')
      table.string(:phone, null: true, comment: 'phone')
    }
  end
end
