class App.RequestsPopupCtrl extends App.RequestsCtrl

  @_import: ['close'].concat(App.RequestsCtrl._import)

  app.controller('RequestsPopupCtrl', @factory())

  initialize: () ->
    super()
    @_scope.request.popup = true
    @_scope.close = @_close

  _completeSubmit: (request) =>
    super(request)
    if request.data.success
      @_close()
