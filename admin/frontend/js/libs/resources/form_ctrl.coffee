class App.ResourcesFormCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$routeParams',
    '$http',
    '$location',
  ]

  initialize: () ->
    @_scope[@constructor._key] = {}
    if @_routeParams.id
      @_http
        .get(@constructor._url + '/' + @_routeParams.id)
        .then((response) =>
          _.extend(@_scope[@constructor._key] , response.data)
        )

    @_scope.submit = @_submit
    @_scope.delete = @_delete

  _submit: () =>
    url = @constructor._url
    if @_routeParams.id
      url += '/' + @_routeParams.id

    @_http
      .post(url, @_scope[@constructor._key])
      .then((response) =>
        if response.data.success
          @_location.path(@constructor._redirect)
          return

        _.extend(@_scope[@constructor._key], response.data)
      )

  _delete: () =>
    @_http
      .delete(@constructor._url + '/' + @_routeParams.id)
      .then((response) =>
        if !response.data.success
          alert('ошибка удаления')
          return

        @_location.path(@constructor._redirect)
      )
