class CreateUserSessions < ActiveRecord::Migration
  def change
    create_table(:user_sessions) { |table|
      table.integer(:user_id)
      table.string(:token)

      table.timestamps(null: false)
    }

    add_foreign_key(:user_sessions, :users, {on_delete: :cascade})
    add_index(:user_sessions, :token, {unique: true})
  end
end
