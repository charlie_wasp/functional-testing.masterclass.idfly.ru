class UserController < ApplicationController

  def initialize(*args)
    @users = UsersService.new
    return super(*args)
  end

  def login
    token, user = @users.authorizate(params[:email], params[:password])
    render({json: {success: true, user: user, token: token}})
  rescue UsersService::Error => error
    render({json: error.as_json})
  end

end
