require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module Capitan

  class Application < Rails::Application
    config.action_view.logger = nil
    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru
    config.active_record.raise_in_transactional_callbacks = true

    config.generators.stylesheets = false
    config.generators.javascripts = false
    config.session_store(:disabled)

    config.autoload_paths += [
      Rails.root.join('app/services'),
      '/main/app/services',
      '/main/app/models',
    ]

    config.secret = ENV['SECRET']
    config.base_href = ENV['BASE_HREF']
  end

end
