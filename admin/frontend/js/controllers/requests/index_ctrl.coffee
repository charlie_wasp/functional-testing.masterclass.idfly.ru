class App.RequestsIndexCtrl extends App.ResourcesIndexCtrl

  app.controller('RequestsIndexCtrl', @factory())

  @_url = 'api/requests'
