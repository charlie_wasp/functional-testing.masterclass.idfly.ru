class ApplicationController < ActionController::Base

  DEFAULT = '<!--# include file="/assets/views/index.html" -->'

  skip_before_action :verify_authenticity_token

  def default
    render text: DEFAULT, layout: nil
  end

end
