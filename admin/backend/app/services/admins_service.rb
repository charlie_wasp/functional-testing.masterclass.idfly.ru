class AdminsService < LoginService

  def initialize
    super
    @model = Admin
    @session = AdminSession
    @session_model = :admin
    @session_model_id = :admin_id
  end

end