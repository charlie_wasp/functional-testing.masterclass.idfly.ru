class App.LoginCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    'close',
  ]

  app.controller('LoginCtrl', @factory())

  initialize: () ->
    @_scope.close = @_close
    @_scope.login = @_login

  _login: () =>
    @_http
      .post('api/user/login', @_scope.user)
      .then((response) =>
        if !response.data.success
          @_scope.user.errors = response.data.errors
          return

        localStorage.setItem('user.token', response.data.token)
        @_rootScope.user = response.data.user
        @_close()
      )
