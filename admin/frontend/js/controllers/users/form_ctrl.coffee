class App.UsersFormCtrl extends App.ResourcesFormCtrl

  app.controller('UsersFormCtrl', @factory())

  @_url = 'api/users'
  @_redirect = 'users'
  @_key = 'user'
