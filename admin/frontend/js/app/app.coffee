@App = {}

@app = angular.module('app', [
  'ngRoute',
  'angularModalService',
])

app.run(($rootScope, $http, $location) ->
  $http.defaults.headers.common['Content-Type'] = 'application/json'
  $http.defaults.headers.common['Accept'] = 'application/json'

  token = localStorage.getItem('admin.token')
  $http.defaults.headers.common['X-Admin-Token'] = token
  $rootScope.adminToken = token

  $http
    .get('api/status')
    .then((response) =>
      _.extend($rootScope, response.data)
      if !response.data.admin
        $location.path('login')
    )
)

app.config ($locationProvider) ->
  $locationProvider.html5Mode(true)

