class App.ResourcesIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$http',
    '$location',
  ]

  initialize: () ->
    @_http
      .get(@constructor._url)
      .then((response) =>
        _.extend(@_scope, response.data)
      )