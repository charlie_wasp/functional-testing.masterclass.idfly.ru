class CreateAdmins < ActiveRecord::Migration

  def change
    create_table(:admins, {comment: 'пользователи'}) { |table|
      table.string(:name, {null: true, default: "", comment: 'Имя'})
      table.string(:email, {null: false, default: "", comment: 'Телефон'})
      table.string(:password, {null: true, default: "", comment: 'Пароль'})

      table.timestamps null: false
    }

    add_index(:admins, :email, {unique: true})
  end

end