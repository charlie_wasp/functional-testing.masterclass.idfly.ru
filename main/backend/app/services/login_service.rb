class LoginService

  class Error < ServiceError; end

  def initialize
    @secrets = SecretsService.new
  end

  def authorizate(email, password)
    errors = {}
    if email.nil? || email.empty?
      errors[:email] = ['email не может быть пустым']
    end

    if password.nil? || password.empty?
      errors[:password] = ['пароль не может быть пустым']
    end

    if !errors.empty?
      raise Error.new('некорректные данные', errors)
    end

    user =
      @model
      .where(email: email, password: @secrets.generate(password))
      .first

    if user.nil?
      raise Error.new('пара email/пароль не найдена в базе данных', {
        email: ['некорректный email или пароль'],
        password: ['некорректный email или пароль'],
      })
    end

    session = @session.create({
      @session_model_id => user.id,
      token: SecureRandom.uuid,
    })

    if !session.persisted?
      raise Error.new('ошибка создания сессии')
    end

    signature = @secrets.generate(session.token)
    token = "#{session.token}/#{signature}"
    return token, user
  end

  def get_authorized_session(token)
    if token.nil?
      return nil
    end

    actual_token, signature = token.split('/')
    if signature != @secrets.generate(actual_token)
      return nil
    end

    session = @session.where(token: actual_token).first
    if session.nil?
      return nil
    end

    return session
  end

  def get_authorized_user(token)
    session = get_authorized_session(token)
    if session.nil?
      return nil
    end

    return session.send(@session_model)
  end

  def require_authorized_user(token)
    user = get_authorized_user(token)
    if user.nil?
      raise Error.new('требуется авторизация')
    end

    return user
  end

end