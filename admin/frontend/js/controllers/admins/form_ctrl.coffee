class App.AdminsFormCtrl extends App.ResourcesFormCtrl

  app.controller('AdminsFormCtrl', @factory())

  @_url = 'api/admins'
  @_redirect = 'admins'
  @_key = 'admin'
