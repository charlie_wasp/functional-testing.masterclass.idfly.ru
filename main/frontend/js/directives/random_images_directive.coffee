app.directive 'randomImages', () ->
  restrict: 'A',
  scope: {
    maxNumber: '=maxNumber',
    randomNumber: '=randomNumber'
  },
  link: (scope, element) ->
    scope.randomNumber = Math.floor(Math.random() * scope.maxNumber) + 1 
