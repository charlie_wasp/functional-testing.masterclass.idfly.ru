class ApplicationController < ActionController::Base

  DEFAULT = "<!--# include file=\"#{Rails.application.config.base_href}"\
    "/assets/views/index.html\" -->"

  skip_before_action(:verify_authenticity_token)
  before_filter(:require_authorization, except: [:default])

  def require_authorization
    token = request.headers['X-Admin-Token']
    if token.nil?
      token = params['X-Admin-Token']
    end

    AdminsService.new.require_authorized_user(token)
  end

  def default
    render({text: DEFAULT, layout: nil})
  end

end
