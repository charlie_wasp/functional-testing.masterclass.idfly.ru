Rails.application.routes.draw do

  # ===
  # API
  # ===

  # status

  get '/api/status' => 'status#status'

  # session

  post '/api/session' => 'session#create'
  delete '/api/session' => 'session#destroy'

  # users

  get '/api/users' => 'users#list'
  get '/api/users/:id' => 'users#get'
  post '/api/users' => 'users#create'
  post '/api/users/:id' => 'users#update'
  delete '/api/users/:id' => 'users#destroy'

  # admins

  get '/api/admins' => 'admins#list'
  get '/api/admins/:id' => 'admins#get'
  post '/api/admins' => 'admins#create'
  post '/api/admins/:id' => 'admins#update'
  delete '/api/admins/:id' => 'admins#destroy'

  # requests

  get '/api/requests' => 'requests#list'
  get '/api/requests/download' => 'requests#download'
  get '/api/requests/:id' => 'requests#get'
  post '/api/requests' => 'requests#create'
  post '/api/requests/:id' => 'requests#update'
  delete '/api/requests/:id' => 'requests#destroy'

  # ======
  # PUBLIC
  # ======

  # index

  root 'application#default'

  # login

  get 'login' => 'application#default'

  # admins

  get 'admins' => 'application#default'
  get 'admins/create' => 'application#default'
  get 'admins/:id' => 'application#default'

  # users

  get 'users' => 'application#default'
  get 'users/create' => 'application#default'
  get 'users/:id' => 'application#default'

  # requests

  get 'requests' => 'application#default'
  get 'requests/create' => 'application#default'
  get 'requests/:id' => 'application#default'

end
