class RequestsController < ResourcesController

  def initialize(*args)
    @model = Request
    return super(*args)
  end

  def download
    require 'csv'
    data = CSV.generate { |csv|
      Request.all.each { |request|
        csv << request.attributes.values
      }
    }

    response.headers['Content-Disposition'] =
      'attachment; filename="requests.csv"'

    render({text: data, content_type: Mime::CSV})
  end

  private

  def _params
    return params.permit(:name, :email, :phone, :is_help_required, :popup)
  end

end
