class SecretsService

  class Error < ServiceError; end

  def generate(string)
    return Digest::MD5.hexdigest("#{Rails.application.config.secret}:#{string}")
  end

end