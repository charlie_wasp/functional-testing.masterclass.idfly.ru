class UsersService < LoginService

  def initialize
    super
    @model = User
    @session = UserSession
    @session_model = :user
    @session_model_id = :user_id
  end

end